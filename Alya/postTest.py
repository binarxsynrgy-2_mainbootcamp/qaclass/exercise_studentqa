zero = int(False)
one = int(True)
hundred = int(f"{one}{zero}{zero}")

def showOneToHundred(i):
    if i <= hundred:
        print(i)
        showOneToHundred(i + one)

print("---------- One To Hundred Numbers ----------")
showOneToHundred(one)